; default stuff that came with the file when created
#SingleInstance, Force
SendMode Input
SetWorkingDir, %A_ScriptDir%

#Persistent
; match beggining of wintitle
SetTitleMatchMode, 1
; detect windows in other virtual monitors
DetectHiddenWindows, On

; calculator button, found this somewhere on the internet - Launch_App2 did not work
; also needed to disable the actions in MS Keyboard app
SC121::

; find open excel windows using its class
if WinExist("ahk_class XLMAIN") {
    WinActivate
}
else {
    run excel.exe
}
return

;; com objects alternative from https://www.autohotkey.com/boards/viewtopic.php?t=61365
; try
; {
;     X1 := ComObjActive("Excel.Application")
;     if WinExist("WinTitle" [, "WinText", "ExcludeTitle", "ExcludeText"])
;     WinActivate, % "ahk_id " X1.Hwnd
;     MsgBox "modo 2!"
;     return
; }