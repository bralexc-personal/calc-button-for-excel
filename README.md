# Excel Calculator Button

Open MS Excel using the calculator button on your keyboard.

## Background

Lots of keyboards have a key dedicated to opening the Windows Calculator app. In many cases, that button could be reassigned to open any other app chosen by the user.

I used to map that to Excel. If I wanted to do some quick math, Excel would be my go to app.

That button would _always_ open a new instance of Excel. On older versions (2007, 2010, 2013) that new instance would open almost instantaneouly. I'd end up with several instances of Excel open on my PC, but I did not care.

Jump forward to Office 365. Excel now takes a few seconds to open - not a lot, but just enough so that, when I press the Calc button, the previouly open Excel instance would pop to the front and, as I start using it, a new Excel instace will open.

And this is where this _very_ simple script for AutoHotKey springs to life. It first searches for any open instance of Excel - if found, make that windows active; if not, open a new instance.

## Usage

1. Place a shortcut to this script in your start-up folder (`shell:startup`). 
2. Press the Calculator button on your keyboard.
3. Do you math in Excel.
4. (...)
5. Profit!

If you have that MS Keyboard app installed (or any other app that does key bindings), make sure that the Calculator button _in that app_ has nothing assigned to it.

## Requirements

Have AutoHotKey installed.
